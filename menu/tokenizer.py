import re

def tokenizer(input_string):
    tokens = []
    current_token = []
    in_quotes = False
    escape_next = False

    for i, char in enumerate(input_string):
        if escape_next:
            current_token.append(char)
            escape_next = False
        elif char == '\\':
            escape_next = True
        elif char == '"':
            in_quotes = not in_quotes
            if in_quotes:
                current_token.append(char)  # Start of quote
            else:
                # End of quote, process as a token if needed
                current_token.append(char)
                token = ''.join(current_token)
                if token.startswith('"') and token.endswith('"'):
                    token = token[1:-1]  # Strip surrounding quotes
                tokens.append(token)
                current_token = []
        elif char.isspace() and not in_quotes:
            if current_token:
                token = ''.join(current_token)
                if ':' in token:
                    key, sep, value = token.partition(':')
                    if value.startswith('"') and value.endswith('"'):
                        value = value[1:-1]  # Strip quotes from value
                    tokens.append(f"{key}:{value}")
                else:
                    tokens.append(token)
                current_token = []
        else:
            current_token.append(char)

    if current_token:
        token = ''.join(current_token)
        if ':' in token:
            key, sep, value = token.partition(':')
            if value.startswith('"') and value.endswith('"'):
                value = value[1:-1]
            tokens.append(f"{key}:{value}")
        else:
            tokens.append(token)

    return tokens

# テストケース
def test_jirorian_tokenizer():
    test_strings = [
        'hello world',
        'hello\\ world',
        'hello "world one" world',
        'hello\\ \"escaped quote\" world',
        '"hello world"',
        'action:"Export to docx"',
        ':valueOnly',
        'key:value',
        'key:"value with spaces"'
    ]
    for test in test_strings:
        print(f"Input: {test}")
        print(f"Output: {tokenizer(test)}\n")


if __name__ == '__main__':
    #test_jirorian_tokenizer()
    line = '/File +New Open Download +pptx pdf -Quit /Edit +Undo Redo Search +Next Prev .'

    #line = '/File //New //Open //Download ///pptx ///pdf //Quit /Edit //Undo //Redo //Search ///Next ///Prev .'
    array = tokenizer(line)
    print(array)


