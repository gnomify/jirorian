import xml.etree.ElementTree as ET
from xml.dom import minidom

import re

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

from tokenizer import tokenizer

class MenuNode:
    def __init__(self, label, parent=None):
        self.label = label
        self.parent = parent
        self.children = []
        self.action = None  # 初期値としてNoneを設定

    def add_child(self, child):
        self.children.append(child)
        self.action = None

    def is_leaf(self):
        return len(self.children) == 0

def print_menu(node, depth=0):
    """ツリー構造を出力する関数"""
    print("  " * depth + f"{node.label} ({getattr(node, 'action', 'None')})")
    for child in node.children:
        print_menu(child, depth + 1)

def parse_keyval(token, label):
    key, value = token.split(':', 1)

    value = value or 'app'
    category, _, method = value.partition('.')

    category = category or 'app'
    if not method:
        label = re.sub(r"[!@#$%^&*()_+\s]", "_", label)
        method = label.lower()

    return f"{category}.{method}"

def parse_token(tokens):
    last_menu = None
    last_token = None
    rootmenu = MenuNode('root')
    current_depth = 0
    menu_array = list(range(100))
    menu_array[0] = rootmenu

    for token in tokens:
        if '.' == token:
            #print_menu(rootmenu)
            return rootmenu

        if ':' in token:
            if last_menu is None:
                continue
            if last_token is None:
                continue

            label = last_menu.label
            action = parse_keyval(token, label) 
            last_token = token
            last_menu.action = action

            #print(last_menu.label, last_menu.action)
            continue

        elif token.startswith('/'):
            label = token.lstrip('/')
            depth = len(token) - len(label)
            menu = MenuNode(label)

            pattern = r"[!@#$%^&*()_+\s]"
            method = re.sub(pattern, "_", label).lower()
            menu.action = f"action:app.{method}"
            #print(depth)
            menu_array[depth - 1].add_child(menu)
            menu_array[depth] = menu
            current_depth = depth
        elif token.startswith('+'):
            depth = current_depth + 1
            label = token[1:]
            menu = MenuNode(label)
            pattern = r"[!@#$%^&*()_+\s]"
            method = re.sub(pattern, "_", label).lower()
            menu.action = f"action:app.{method}"
            menu_array[depth] = menu
            last_menu.add_child(menu)
            current_depth = depth
        elif token.startswith('-'):
            label = token.lstrip('-')
            depth = current_depth + len(label) - len(token)
            menu = MenuNode(label)
            pattern = r"[!@#$%^&*()_+\s]"
            method = re.sub(pattern, "_", label).lower()
            menu.action = f"action:app.{method}"

            menu_array[depth - 1].add_child(menu)
            menu_array[depth] = menu
            current_depth = depth
        else:
            menu = MenuNode(token)
            pattern = r"[!@#$%^&*()_+\s]"
            method = re.sub(pattern, "_", token).lower()
            menu.action = f"action:app.{method}"

            menu_array[current_depth - 1].add_child(menu)
            menu_array[current_depth] = menu

        last_token = token
        last_menu = menu

    #print_menu(rootmenu)
    return rootmenu

def build_xml(node):
    if node.label == "root":
        interface = ET.Element("interface")
        menu = ET.SubElement(interface, "menu", id="menubar")
        for child in node.children:
            build_node(child, menu)
        return interface
    return None


def build_node(node, parent_element):
    if node.is_leaf():
        item = ET.SubElement(parent_element, "item")
        ET.SubElement(item, "attribute", name="label").text = node.label
        ET.SubElement(item, "attribute", name="action").text = node.action
    else:
        submenu = ET.SubElement(parent_element, "submenu")
        ET.SubElement(submenu, "attribute", name="label").text = node.label
        for child in node.children:
            build_node(child, submenu)


def prettify_xml(element):
    rough_string = ET.tostring(element, encoding="utf-8")
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")


# add_from_call メソッド
def add_from_call(self, content):
    if not isinstance(content, str):
        raise TypeError(f"'content' 引数には文字列を指定してください。渡された型: {type(content).__name__}")
    #print(f"add_from_call received content:\n{content}")
    menu_tokens = tokenizer(content)
    parsed_root = parse_token(menu_tokens)
    xml_tree = build_xml(parsed_root)
    xml_output = prettify_xml(xml_tree)
    self.add_from_string(xml_output)


# add_from_call_file メソッド
def add_from_call_file(self, file_path):
    if not isinstance(file_path, str):
        raise TypeError(f"'file_path' 引数には文字列を指定してください。渡された型: {type(file_path).__name__}")
    
    try:
        with open(file_path, "r", encoding="utf-8") as file:
            content = file.read()  # ファイルの中身を読み取る
    except FileNotFoundError:
        raise FileNotFoundError(f"指定されたファイルが見つかりません: {file_path}")
    except IOError as e:
        raise IOError(f"ファイルを読み取る際にエラーが発生しました: {e}")
    
    # ファイルの中身を add_from_call に渡す
    self.add_from_call(content)

# Gtk.Builder クラスにメソッドを追加
Gtk.Builder.add_from_call = add_from_call
Gtk.Builder.add_from_call_file = add_from_call_file


if __name__ == "__main__":
    # テストコード
    menu_tokens = [
        '/File', '+New', 'Open', 'action:app.open', 'Download',              
        '+pptx', 'action:app.export_pptx', 'pdf', 'action:app.export_pdf',
        '-Quit', 'action:app.quit', '/Edit', '+Undo', 'action:', 'Redo',
        ':win.redo', 'Search', '+Next', 'action:win', 'Prev', ':win', '.'
    ]

    line = '/File +New Open action:app.open Download +pptx action:app.export_pptx pdf action:app.export_pdf -Quit action:app.quit /Edit +Undo action: Redo :win.redo Search +Next action:win Prev :win .'

    menu_tokens = tokenizer(line)

    parsed_root = parse_token(menu_tokens)
#    print(parsed_root)

    xml_tree = build_xml(parsed_root)

    xml_string = prettify_xml(xml_tree)

    xml_string = f"<?xml version='1.0' encoding='utf-8'?>\n{''.join(xml_string.splitlines(keepends=True)[1:])}"


    print(xml_string)

