import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib, Gio

import jirorian

class MyApplication(Gtk.Application):

    def __init__(self):
        super().__init__(application_id="org.example.myapp",
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.builder = Gtk.Builder()

        self.builder.add_from_file("app.ui")
        #self.builder.add_from_file("menu.ui")
        self.builder.add_from_call("/File +New Open action:app.open Download +pptx action:app.export_pptx pdf action:app.export_pdf -Quit action:app.quit /Edit +Undo action: Redo :win.redo Search +Next action:win Prev :win .")

        self.connect("startup", self.on_startup)
        self.connect("activate", self.on_activate)

    def on_startup(self, app):
#        Gtk.Application.do_startup(self)

        if not hasattr(self, 'window'):
            self.window = self.builder.get_object("main")
            self.window.set_title("My Application")
            self.window.set_default_size(400, 300)
            self.window.set_application(app)
            menubar = self.builder.get_object('menubar')
            self.set_menubar(menubar)
            self.window.set_show_menubar(True)

        self.window.show()

    def on_activate(self, app):
        self.window.present()

def main():
    app = MyApplication()

    app.run(None)

if __name__ == "__main__":
    main()

