#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define BUFFER_SIZE 1024

void parse_and_print_tokens(const char *text) {
    const char *ptr = text;
    while (*ptr) {
        // スキップ空白
        while (isspace(*ptr)) {
            ptr++;
        }

        if (*ptr == '\0') {
            break;
        }

        // 引用符で囲まれた文字列を処理
        if (*ptr == '"') {
            const char *start = ptr++;
            while (*ptr && (*ptr != '"' || (*(ptr-1) == '\\' && *(ptr-2) != '\\'))) {
                ptr++;
            }
            if (*ptr == '"') {
                ptr++;
            }
            printf("%.*s,\n", (int)(ptr - start), start);
        } 
        // 文字列を処理
        else if (isalpha(*ptr) || *ptr == ':') {
            const char *start = ptr;
            while (isalnum(*ptr) || *ptr == ':') {
                ptr++;
            }
            printf("%.*s,\n", (int)(ptr - start), start);
        } 
        // ピリオドを処理
        else if (*ptr == '.') {
            printf(".\n");
            ptr++;
        } 
        // 未知の文字はスキップ
        else {
            ptr++;
        }
    }
}

int main(int argc, char *argv[]) {
    char buffer[BUFFER_SIZE];
    char *text = NULL;
    size_t text_length = 0;

    // コマンドライン引数の処理
    if (argc > 1) {
        FILE *file = fopen(argv[1], "r");
        if (!file) {
            fprintf(stderr, "ファイルを開くことができません: %s\n", argv[1]);
            return 1;
        }
        fseek(file, 0, SEEK_END);
        text_length = ftell(file);
        fseek(file, 0, SEEK_SET);
        text = malloc(text_length + 1);
        fread(text, 1, text_length, file);
        text[text_length] = '\0';
        fclose(file);
    } else {
        size_t total_length = 0;
        while (fgets(buffer, BUFFER_SIZE, stdin)) {
            size_t buffer_length = strlen(buffer);
            text = realloc(text, total_length + buffer_length + 1);
            memcpy(text + total_length, buffer, buffer_length);
            total_length += buffer_length;
        }
        if (text) {
            text[total_length] = '\0';
        }
    }

    if (text) {
        parse_and_print_tokens(text);
        free(text);
    }

    return 0;
}
