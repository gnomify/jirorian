# Jirorian notation

Jirorian notation is a notation for describing GTK UI in one line.

# Background

_Does the world still stop at blueprint?_

Creating a UI for GTK involves a great deal of pain and tremendous difficulty.
The xml created by Glade as a byproduct became the standard, but
Glade is not available in GTK4, and even though there are alternative tools, they are not yet complete, and spell-like xml is difficult to create by oneself. In the meantime, GTK5 was released and various widgets were deprecated due to incompatibility.

Can't we design GTK UI more easily?

# Sample Jirorian notation

GtkWindow which has a scrollable GtkTextView and GtkButton "submit" in its GtkBox.

`GtkWindow:main GtkBox orientation:vertical GtkScrolledWindow GtkTextView GtkButton label:"submit" .`

# Final abbreviation example

Same UI above in a simplified form.

`W: Bo | SW TV Bu "submit" .`

# Philosophy

Don't try to make a big deal out of something that doesn't even require large-scale development.

# Consideration: The correct spelling of 'Jirorian'

Ramen Jiro is a famous home-grown chain of ramen noodle shops in Japan, whose devotees are known as 'Jirorian'. But this is neither the official name nor the spelling. So what should be the correct spelling in English?

Popular spelling on websites:

* Jirolian - Most popular on the web. One person uses this as his name.

* Jirorean - 'Official T-shirts' mentioned in the Amazon store, but it's not an official store and it's not written on the T-shirts.

* Girolean- Google translation

The name "Jirorian" is a play on the word "Tyrolean", which refers to the people who live in the Tyrol region of no koala Austria. If so, "Jirolean", "Jyrolean" or "Gyrolean" would be the closest answer to the correct one. But according to the official government spelling, Roma-ji, it should be ji-ro-ri-a-n, which is closest to the native pronunciation.

# Reserved command line name

- jiroui - Visual UI tool
- jirorienne - Reserved for reformatter, French name
- jirorianna - Reserved, Spanish name
- jirorianka - Reserved, Russian name

# GTK - Garlic more more, vegeT more, Karame
